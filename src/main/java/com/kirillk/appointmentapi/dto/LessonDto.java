package com.kirillk.appointmentapi.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
public class LessonDto {

    private Long id;

    @NotEmpty
    private String lessonName;
    @NotEmpty
    private LocalDate date;
    @NotEmpty
    private LocalTime timeFrom;
    @NotEmpty
    private LocalTime timeTo;
    @NotEmpty
    private Double price;
    private TeacherDto teacherDto;

    public LessonDto(@NotEmpty String lessonName,@NotEmpty LocalDate date, @NotEmpty LocalTime timeFrom,
                     @NotEmpty LocalTime timeTo, @NotEmpty Double price, TeacherDto teacherDto) {
        this.lessonName = lessonName;
        this.date = date;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.price = price;
        this.teacherDto=teacherDto;
    }
}
