package com.kirillk.appointmentapi.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class TeacherDto {

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @Email
    private String email;
    @NotEmpty
    private String password;
    private List<LessonDto> lessons;
    @Temporal(TemporalType.DATE)
    private Date birthday;

    public TeacherDto(@NotEmpty String firstName, @NotEmpty String lastName, @Email String email,
                      @NotEmpty String password, List<LessonDto> lessons, Date birthday) {
        this.firstName = firstName;
        this.lastName= lastName;
        this.email = email;
        this.password = password;
        this.lessons = lessons;
        this.birthday = birthday;
    }
}
