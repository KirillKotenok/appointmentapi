package com.kirillk.appointmentapi.dto;

import com.kirillk.appointmentapi.entity.Lesson;
import com.kirillk.appointmentapi.entity.Student;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class StudentsHasLessonDto {
    private Long id;
    @NotEmpty
    private Student student;
    @NotEmpty
    private Lesson lesson;

    public StudentsHasLessonDto(Student student, Lesson lesson) {
        this.student = student;
        this.lesson = lesson;
    }
}
