package com.kirillk.appointmentapi.repo;

import com.kirillk.appointmentapi.entity.StudentsHasLesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentsHasLessonRepo extends JpaRepository<StudentsHasLesson, Long> {
 StudentsHasLesson findAllByLesson_Id (Long lessonId);
}
