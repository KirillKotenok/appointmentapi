package com.kirillk.appointmentapi.repo;

import com.kirillk.appointmentapi.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepo extends JpaRepository<Teacher, Long> {
    Teacher findAllByLessonsId (Long lessonId);
}
