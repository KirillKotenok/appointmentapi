package com.kirillk.appointmentapi.repo;

import com.kirillk.appointmentapi.entity.Lesson;
import com.kirillk.appointmentapi.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Repository
public interface LessonRepo extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByDate (LocalDate date);

    List<Lesson> findAllByTeacher (Teacher teacher);

    @Query("select l from Lesson as l join Teacher as t on l.teacher.id= :teacherId " +
            "where l.date = :date and :timeFrom between l.timeFrom and l.timeTo " +
            "or :timeTo between l.timeFrom and l.timeTo " +
            "or l.timeFrom = :timeFrom and l.timeTo = :timeTo")
    boolean hasLessons (@Param("teacherId") Long id,
                        @Param("timeFrom") LocalTime timeFrom,
                        @Param("timeTo") LocalTime timeTo,
                        @Param("date") LocalDate date);
}
