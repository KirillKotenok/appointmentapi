package com.kirillk.appointmentapi.exception;

public class TimeOverlayException extends RuntimeException{
    public TimeOverlayException(){}

    public TimeOverlayException(String message){
        super(message);
    }
}
