package com.kirillk.appointmentapi.exception;

public class NotFoundInDbException extends RuntimeException {
    public NotFoundInDbException(){};

    public NotFoundInDbException(String message){super(message);};
}
