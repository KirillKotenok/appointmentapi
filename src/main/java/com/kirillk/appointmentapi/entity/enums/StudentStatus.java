package com.kirillk.appointmentapi.entity.enums;

public enum StudentStatus {
    APPROVED,
    DECLINED,
    UNDER_CONSIDERATION
}
