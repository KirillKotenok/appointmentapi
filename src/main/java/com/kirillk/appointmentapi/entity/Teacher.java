package com.kirillk.appointmentapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


@Data
@NoArgsConstructor
@Entity
@Table(name = "teacher")
public class Teacher extends People{

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher")
    private List<Lesson> lessons;
    public Teacher(List<Lesson> lessons) {
    this.lessons=lessons;
    }
}
