package com.kirillk.appointmentapi.entity;

import com.kirillk.appointmentapi.entity.enums.StudentStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
@NoArgsConstructor
@Table(name = "student_has_lesson", uniqueConstraints = @UniqueConstraint(columnNames = {"student_id", "lesson_id"}))
public class StudentsHasLesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @NotEmpty
    private Student student;
    @ManyToOne
    @NotEmpty
    private Lesson lesson;
    @Enumerated(EnumType.STRING)
    @NotEmpty
    private StudentStatus status = StudentStatus.UNDER_CONSIDERATION;

    public StudentsHasLesson(Student student, Lesson lesson) {
    this.lesson=lesson;
    this.student=student;
    }
}
