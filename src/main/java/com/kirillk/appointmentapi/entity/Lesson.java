package com.kirillk.appointmentapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor
@Data
@Entity
@Table(name = "lesson")
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty
    private String lessonName;
    @NotEmpty
    private String description;
    @NotEmpty
    private LocalDate date;
    @NotEmpty
    private LocalTime timeFrom;
    @NotEmpty
    private LocalTime timeTo;
    @NotEmpty
    private Double price;
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    public Lesson(LocalDate date, LocalTime timeFrom, LocalTime timeTo,
                  Double price, Teacher teacher) {
        this.date= date;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.price=price;
        this.teacher=teacher;
    }
}
