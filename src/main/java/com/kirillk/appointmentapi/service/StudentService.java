package com.kirillk.appointmentapi.service;

import com.kirillk.appointmentapi.dto.StudentDto;

import java.util.List;

public interface StudentService {
    StudentDto saveStudent(StudentDto studentDto);

    StudentDto updateStudent(StudentDto studentDto);

    StudentDto findStudentById(Long id);

    void deleteStudentById (Long id);

    List<StudentDto> findAll();
}
