package com.kirillk.appointmentapi.service;

public interface EncoderService {
    String encode (String password);
}
