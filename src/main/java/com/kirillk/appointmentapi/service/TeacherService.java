package com.kirillk.appointmentapi.service;

import com.kirillk.appointmentapi.dto.TeacherDto;
import com.kirillk.appointmentapi.dto.LessonDto;

import java.util.List;

public interface TeacherService {

    TeacherDto saveTeacher (TeacherDto teacherDto);

    TeacherDto updateTeacher (TeacherDto teacherDto);

    void deleteTeacherByID (Long id);

    TeacherDto findTeacherById(Long id);

    List<TeacherDto> findAll ();

    TeacherDto setLessonById(LessonDto lessonDto, Long id);
}
