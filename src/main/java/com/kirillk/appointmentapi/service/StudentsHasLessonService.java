package com.kirillk.appointmentapi.service;

import com.kirillk.appointmentapi.dto.StudentsHasLessonDto;
import org.springframework.web.bind.annotation.PathVariable;

public interface StudentsHasLessonService {
    StudentsHasLessonDto save (StudentsHasLessonDto studentsHasLessonDto);

    StudentsHasLessonDto signUpForALessonAndSave (StudentsHasLessonDto studentsHasLessonDto);

    StudentsHasLessonDto update (StudentsHasLessonDto studentsHasLessonDto);

    StudentsHasLessonDto findById (Long studentsForLessonId);

    StudentsHasLessonDto findByLessonId (Long lessonId);

    void deleteById (Long studentsForLessonId);

    void canceledLessonAndDeleteById (Long studentsFroLessonId);

    StudentsHasLessonDto approveStudentToLesson(Long lessonId);

    StudentsHasLessonDto declineStudentToLesson(Long lessonId);
}
