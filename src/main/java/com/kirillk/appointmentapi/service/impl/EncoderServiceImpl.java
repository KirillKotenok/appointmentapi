package com.kirillk.appointmentapi.service.impl;

import com.kirillk.appointmentapi.service.EncoderService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EncoderServiceImpl implements EncoderService {
    private final transient PasswordEncoder passwordEncoder;

    @Override
    public String encode(String password) {
        String encodingPass = passwordEncoder.encode(password);
        return encodingPass;
    }
}
