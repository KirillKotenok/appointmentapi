package com.kirillk.appointmentapi.service.impl;

import com.kirillk.appointmentapi.dto.LessonDto;
import com.kirillk.appointmentapi.entity.Lesson;
import com.kirillk.appointmentapi.exception.NotFoundInDbException;
import com.kirillk.appointmentapi.exception.TimeOverlayException;
import com.kirillk.appointmentapi.mapper.LessonMapper;
import com.kirillk.appointmentapi.mapper.TeacherMapper;
import com.kirillk.appointmentapi.repo.LessonRepo;
import com.kirillk.appointmentapi.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {
    private final transient LessonRepo lessonRepo;
    private final transient LessonMapper lessonMapper;
    private final transient TeacherMapper teacherMapper;

    @Override
    public LessonDto saveLesson(LessonDto timeAndPriceDto) {
        if (isTimeOverlay(timeAndPriceDto)){
            throw  new TimeOverlayException();
        }else {
            Lesson saved = lessonRepo.save(lessonMapper.toEntity(timeAndPriceDto));
            return lessonMapper.toDto(saved);
        }
    }

    @Override
    public LessonDto updateLesson(LessonDto timeAndPriceDto) {
        if (isTimeOverlay(timeAndPriceDto)){
            throw  new TimeOverlayException();
        }else {
            Lesson updated = lessonRepo.save(lessonMapper.toEntity(timeAndPriceDto));
            return lessonMapper.toDto(updated);
        }
    }

    @Override
    public LessonDto getLesson(Long id) {
        Lesson fromDb = lessonRepo.findById(id).
                orElseThrow(() -> new NotFoundInDbException("Work time and price by this id not found"));
        return lessonMapper.toDto(fromDb);
    }

    @Override
    public List<LessonDto> getLesson() {
        return lessonMapper.toDtoList(lessonRepo.findAll());
    }

    @Override
    public void deleteLesson(Long id) {
        lessonRepo.deleteById(id);
    }


    private boolean isTimeOverlay (LessonDto lessonDto){
        boolean hasLessons = lessonRepo.hasLessons(
                teacherMapper.toEntity(lessonDto.getTeacherDto()).getId(), lessonDto.getTimeFrom(),
                lessonDto.getTimeTo(), lessonDto.getDate());
        return hasLessons;
    }
}
