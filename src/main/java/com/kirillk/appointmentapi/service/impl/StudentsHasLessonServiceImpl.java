package com.kirillk.appointmentapi.service.impl;

import com.kirillk.appointmentapi.dto.StudentsHasLessonDto;
import com.kirillk.appointmentapi.entity.StudentsHasLesson;
import com.kirillk.appointmentapi.entity.Teacher;
import com.kirillk.appointmentapi.entity.enums.StudentStatus;
import com.kirillk.appointmentapi.exception.NotFoundInDbException;
import com.kirillk.appointmentapi.mapper.StudentsHasLessonMapper;
import com.kirillk.appointmentapi.repo.StudentsHasLessonRepo;
import com.kirillk.appointmentapi.repo.TeacherRepo;
import com.kirillk.appointmentapi.service.MailService;
import com.kirillk.appointmentapi.service.StudentsHasLessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentsHasLessonServiceImpl implements StudentsHasLessonService {
    private transient final StudentsHasLessonMapper mapper;
    private transient final StudentsHasLessonRepo repo;
    private transient final MailService mailService;
    private transient final TeacherRepo teacherRepo;

    @Override
    public StudentsHasLessonDto save(StudentsHasLessonDto studentsHasLessonDto) {
        StudentsHasLesson entity = mapper.toEntity(studentsHasLessonDto);
        StudentsHasLesson savedEntity = repo.save(entity);
        StudentsHasLessonDto savedDto = mapper.toDto(savedEntity);
        return savedDto;
    }

    @Override
    public StudentsHasLessonDto signUpForALessonAndSave(StudentsHasLessonDto studentsHasLessonDto) {
        StudentsHasLesson entity = mapper.toEntity(studentsHasLessonDto);
        StudentsHasLesson savedEntity = repo.save(entity);
        StudentsHasLessonDto savedDto = mapper.toDto(savedEntity);

        Teacher teacherByLessonId = teacherRepo.findAllByLessonsId(savedEntity.getLesson().getId());
        mailService.send(teacherByLessonId.getEmail(), savedEntity.getLesson().getLessonName()+" lesson",
                "Student " + savedEntity.getStudent().getFirstName()+ "\\s" + savedEntity.getStudent().getLastName()
                        + " signed up on " + savedEntity.getLesson().getLessonName()+" lessons.");

        return savedDto;
    }

    @Override
    public StudentsHasLessonDto update(StudentsHasLessonDto studentsHasLessonDto) {
        StudentsHasLesson entity = mapper.toEntity(studentsHasLessonDto);
        StudentsHasLesson updatedEntity = repo.save(entity);
        StudentsHasLessonDto updatedDto = mapper.toDto(updatedEntity);
        return updatedDto;
    }

    @Override
    public StudentsHasLessonDto findById(Long studentsForLessonId) {
        StudentsHasLesson fromDb = repo.findById(studentsForLessonId)
                .orElseThrow(()-> new NotFoundInDbException("Students for lesson not found"));
        StudentsHasLessonDto dto = mapper.toDto(fromDb);
        return dto;
    }

    @Override
    public StudentsHasLessonDto findByLessonId(Long lessonId) {
        StudentsHasLesson fromDb = repo.findAllByLesson_Id(lessonId);
        StudentsHasLessonDto dto = mapper.toDto(fromDb);
        return dto;
    }

    @Override
    public void deleteById(Long studentForLessonId) {
        repo.deleteById(studentForLessonId);
    }

    @Override
    public void canceledLessonAndDeleteById(Long studentForLessonId) {
        StudentsHasLesson canceled = repo.findAllByLesson_Id(studentForLessonId);
        repo.deleteById(studentForLessonId);

        Teacher teacherByLessonId = teacherRepo.findAllByLessonsId(studentForLessonId);
        mailService.send(teacherByLessonId.getEmail(), canceled.getLesson().getLessonName()+" lesson",
                "Student " + canceled.getStudent().getFirstName()+ "\\s" + canceled.getStudent().getLastName()
                        + " canceled registration on " + canceled.getLesson().getLessonName()+" lessons.");
    }


    @Override
    public StudentsHasLessonDto approveStudentToLesson(Long lessonId) {
        StudentsHasLesson fromDb = repo.findAllByLesson_Id(lessonId);
        fromDb.setStatus(StudentStatus.APPROVED);
        StudentsHasLesson approved = repo.save(fromDb);
        StudentsHasLessonDto approvedDto = mapper.toDto(approved);

        mailService.send(approved.getStudent().getEmail(), approved.getLesson().getLessonName()+" lesson", "Teacher approve your request.");
        return approvedDto;
    }

    @Override
    public StudentsHasLessonDto declineStudentToLesson(Long lessonId) {
        StudentsHasLesson fromDb = repo.findAllByLesson_Id(lessonId);
        fromDb.setStatus(StudentStatus.DECLINED);
        StudentsHasLesson declined = repo.save(fromDb);
        StudentsHasLessonDto declinedDto = mapper.toDto(declined);

        mailService.send(declined.getStudent().getEmail(), declined.getLesson().getLessonName()+" lesson", "Teacher decline your request.");
        return declinedDto;
    }
}
