package com.kirillk.appointmentapi.service.impl;

import com.kirillk.appointmentapi.dto.TeacherDto;
import com.kirillk.appointmentapi.dto.LessonDto;
import com.kirillk.appointmentapi.entity.Teacher;
import com.kirillk.appointmentapi.exception.NotFoundInDbException;
import com.kirillk.appointmentapi.mapper.TeacherMapper;
import com.kirillk.appointmentapi.repo.TeacherRepo;
import com.kirillk.appointmentapi.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private transient final TeacherRepo teacherRepo;
    private transient final TeacherMapper teacherMapper;

    @Override
    public TeacherDto saveTeacher (TeacherDto teacherDto){
        Teacher saved = teacherRepo.save(teacherMapper.toEntity(teacherDto));
        return teacherMapper.toDto(saved);
    }

    @Override
    public TeacherDto updateTeacher (TeacherDto teacherDto){
        Teacher updated = teacherRepo.save(teacherMapper.toEntity(teacherDto));
        return teacherMapper.toDto(updated);
    }

    @Override
    public void deleteTeacherByID(Long id) {
        teacherRepo.deleteById(id);
    }

    @Override
    public TeacherDto findTeacherById (Long id){
        return teacherMapper.toDto(teacherRepo.findById(id)
                .orElseThrow(() -> new NotFoundInDbException("Don`t found")));
    }

    @Override
    public List<TeacherDto> findAll() {
        return teacherMapper.toDtoList(teacherRepo.findAll());
    }

    @Override
    public TeacherDto setLessonById(LessonDto lessonDto, Long id) {
        TeacherDto teacherFromDb = teacherMapper.toDto(teacherRepo.findById(id).
                orElseThrow(() -> new NotFoundInDbException("Teacher with this Id not found")));
        teacherFromDb.getLessons().add(lessonDto);
        Teacher updatedTeacher = teacherRepo.save(teacherMapper.toEntity(teacherFromDb));
        return teacherMapper.toDto(updatedTeacher);
    }

}
