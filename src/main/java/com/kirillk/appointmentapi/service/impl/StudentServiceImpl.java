package com.kirillk.appointmentapi.service.impl;

import com.kirillk.appointmentapi.dto.StudentDto;
import com.kirillk.appointmentapi.entity.Student;
import com.kirillk.appointmentapi.exception.NotFoundInDbException;
import com.kirillk.appointmentapi.mapper.StudentMapper;
import com.kirillk.appointmentapi.repo.StudentRepo;
import com.kirillk.appointmentapi.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private transient final StudentRepo studentRepo;
    private transient final StudentMapper studentMapper;


    @Override
    public StudentDto saveStudent(StudentDto studentDto) {
        Student savedUser = studentRepo.save(studentMapper.toEntity(studentDto));
        return studentMapper.toDto(savedUser);
    }

    @Override
    public StudentDto updateStudent(StudentDto studentDto) {
        Student savedUser = studentRepo.save(studentMapper.toEntity(studentDto));
        return studentMapper.toDto(savedUser);
    }

    @Override
    public StudentDto findStudentById(Long id) {
        Student userFromDb = studentRepo.findById(id)
                .orElseThrow(() -> new NotFoundInDbException("User by this id not found"));
        return studentMapper.toDto(userFromDb);
    }

    @Override
    public void deleteStudentById(Long id) {
        studentRepo.deleteById(id);
    }

    @Override
    public List<StudentDto> findAll() {
        return studentMapper.toDtoList(studentRepo.findAll());
    }
}
