package com.kirillk.appointmentapi.service;

public interface MailService {
    void send(String mailTo, String subject, String message);
}
