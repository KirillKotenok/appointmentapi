package com.kirillk.appointmentapi.service;

import com.kirillk.appointmentapi.dto.StudentDto;
import com.kirillk.appointmentapi.dto.LessonDto;

import java.util.List;

public interface LessonService {

    LessonDto saveLesson(LessonDto timeAndPriceDto);

    LessonDto updateLesson(LessonDto timeAndPriceDto);

    LessonDto getLesson (Long id);

    List<LessonDto> getLesson();

    void deleteLesson (Long id);

}

