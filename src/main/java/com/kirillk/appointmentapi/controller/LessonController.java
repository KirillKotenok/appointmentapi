package com.kirillk.appointmentapi.controller;

import com.kirillk.appointmentapi.dto.LessonDto;
import com.kirillk.appointmentapi.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher/lesson")
@RequiredArgsConstructor
public class LessonController {
    private final transient LessonService lessonService;

    @PostMapping("/save")
    public ResponseEntity<LessonDto> saveLesson(@RequestBody LessonDto lessonDto){
        if (!lessonDto.getTimeFrom().isAfter(lessonDto.getTimeTo())) {
            LessonDto savedTimeAndPrice = lessonService.saveLesson(lessonDto);
            return ResponseEntity.ok(savedTimeAndPrice);
        }else return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/update")
    public ResponseEntity<LessonDto> updateLesson(@RequestBody LessonDto lessonDto){
        if (!lessonDto.getTimeFrom().isAfter(lessonDto.getTimeTo())) {
            LessonDto updatedTimeAndPrice = lessonService.updateLesson(lessonDto);
            return ResponseEntity.ok(updatedTimeAndPrice);
        }else return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<LessonDto> getLessonById(@PathVariable Long id){
        LessonDto fromDb = lessonService.getLesson(id);
        return ResponseEntity.ok(fromDb);
    }
    @GetMapping("/get/all")
    public ResponseEntity<List<LessonDto>> getAllLessons (){
        List<LessonDto> all = lessonService.getLesson();
        return ResponseEntity.ok(all);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteLessonById(@PathVariable Long id){
        lessonService.deleteLesson(id);
        return new ResponseEntity<String>("Delete successful", HttpStatus.OK);
    }
}
