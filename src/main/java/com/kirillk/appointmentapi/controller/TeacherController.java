package com.kirillk.appointmentapi.controller;

import com.kirillk.appointmentapi.dto.TeacherDto;
import com.kirillk.appointmentapi.service.EncoderService;
import com.kirillk.appointmentapi.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {
    private transient final EncoderService encoderService;
    private transient final TeacherService teacherService;

    @PostMapping("/save")
    public ResponseEntity<TeacherDto> saveTeacher (@RequestBody TeacherDto teacherDto){
        String encodePassword = encoderService.encode(teacherDto.getPassword());
        teacherDto.setPassword(encodePassword);
        TeacherDto savedTeacher = teacherService.saveTeacher(teacherDto);
        return ResponseEntity.ok(savedTeacher);
    }

    @PutMapping("/update")
    public ResponseEntity<TeacherDto> updateTeacher (@RequestBody TeacherDto teacherDto){
        String encodePassword = encoderService.encode(teacherDto.getPassword());
        teacherDto.setPassword(encodePassword);
        TeacherDto updatedTeacher= teacherService.updateTeacher(teacherDto);
        return ResponseEntity.ok(updatedTeacher);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<TeacherDto> getTeacherById(@PathVariable Long id){
        TeacherDto teacher = teacherService.findTeacherById(id);
        return ResponseEntity.ok(teacher);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<TeacherDto>> getAll (){
        return ResponseEntity.ok(teacherService.findAll());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteTeacherByID(@PathVariable Long id){
        teacherService.deleteTeacherByID(id);
        return new ResponseEntity<String>("Delete successful", HttpStatus.OK);
    }

}
