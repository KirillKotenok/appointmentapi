package com.kirillk.appointmentapi.controller;

import com.kirillk.appointmentapi.dto.StudentsHasLessonDto;
import com.kirillk.appointmentapi.service.StudentsHasLessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController("/student_on_lesson")
@RequiredArgsConstructor
public class StudentHasLessonController {
    private transient final StudentsHasLessonService studentsHasLessonService;

    @PostMapping("/save")
    public ResponseEntity<StudentsHasLessonDto> saveStudentHasLesson (@RequestBody StudentsHasLessonDto studentsHasLessonDto){
        StudentsHasLessonDto saved = studentsHasLessonService.save(studentsHasLessonDto);
        return ResponseEntity.ok(saved);
    }

    @PostMapping("/signUp")
    public ResponseEntity<StudentsHasLessonDto> signUpOnLesson (@RequestBody StudentsHasLessonDto studentsHasLessonDto){
        StudentsHasLessonDto saved = studentsHasLessonService.signUpForALessonAndSave(studentsHasLessonDto);
        return ResponseEntity.ok(saved);
    }

    @PutMapping("update")
    public ResponseEntity<StudentsHasLessonDto> updateStudentHasLesson (@RequestBody StudentsHasLessonDto studentsHasLessonDto){
        StudentsHasLessonDto updated = studentsHasLessonService.update(studentsHasLessonDto);
        return ResponseEntity.ok(updated);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteStudentHasLessonById (@PathVariable Long id){
        studentsHasLessonService.deleteById(id);
        return new ResponseEntity<>("Delete successful", HttpStatus.OK);
    }

    @DeleteMapping("/cancel/{id}")
    public ResponseEntity<String> cancelLessonById (@PathVariable Long id){
        studentsHasLessonService.canceledLessonAndDeleteById(id);
        return new ResponseEntity<>("Cancel successful", HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<StudentsHasLessonDto> getStudentHasLessonById(@PathVariable Long id){
        StudentsHasLessonDto fromDb = studentsHasLessonService.findById(id);
        return ResponseEntity.ok(fromDb);
    }

    @GetMapping("/find_by_lesson_id/{id}")
    public ResponseEntity<StudentsHasLessonDto> getStudentHasLessonByLessonId(@PathVariable Long lessonId){
        StudentsHasLessonDto fromDb = studentsHasLessonService.findByLessonId(lessonId);
        return ResponseEntity.ok(fromDb);
    }

    @PutMapping("/approve/{id}")
    public ResponseEntity<StudentsHasLessonDto> approveStudentForLessonByLessonId (@PathVariable Long lessonId){
        StudentsHasLessonDto approvedLessonWithStudent = studentsHasLessonService.approveStudentToLesson(lessonId);
        return ResponseEntity.ok(approvedLessonWithStudent);
    }

    @PutMapping("/decline/{id}")
    public ResponseEntity<StudentsHasLessonDto> declinedStudentForLessonByLessonId (@PathVariable Long lessonId){
        StudentsHasLessonDto declinedLessonWithStudent = studentsHasLessonService.declineStudentToLesson(lessonId);
        return ResponseEntity.ok(declinedLessonWithStudent);
    }
}
