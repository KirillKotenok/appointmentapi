package com.kirillk.appointmentapi.controller;

import com.kirillk.appointmentapi.dto.StudentDto;
import com.kirillk.appointmentapi.service.EncoderService;
import com.kirillk.appointmentapi.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private transient final EncoderService encoderService;
    private transient final StudentService studentService;

    @PostMapping("/save")
    public ResponseEntity<StudentDto> saveStudent (@RequestBody final StudentDto studentDto){
        String encodePassword = encoderService.encode(studentDto.getPassword());
        studentDto.setPassword(encodePassword);
        StudentDto saved = studentService.saveStudent(studentDto);
        return new ResponseEntity<StudentDto>(saved, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<StudentDto> updateStudent(@RequestBody final StudentDto studentDto){
        String encodePassword = encoderService.encode(studentDto.getPassword());
        studentDto.setPassword(encodePassword);
        StudentDto updated = studentService.updateStudent(studentDto);
        return new ResponseEntity<StudentDto>(updated, HttpStatus.OK);
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<StudentDto>> getAll (){
        return ResponseEntity.ok(studentService.findAll());
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<StudentDto> getById(@PathVariable Long id){
        return ResponseEntity.ok(studentService.findStudentById(id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteStudentById (@PathVariable Long id){
        studentService.deleteStudentById(id);
        return new ResponseEntity<String>("Delete successful", HttpStatus.OK);
    }


}
