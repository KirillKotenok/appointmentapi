package com.kirillk.appointmentapi.mapper;

import com.kirillk.appointmentapi.dto.StudentDto;
import com.kirillk.appointmentapi.entity.Student;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {
    Student toEntity(StudentDto studentDto);
    StudentDto toDto (Student student);
    List<StudentDto> toDtoList (List<Student> studentList);
}
