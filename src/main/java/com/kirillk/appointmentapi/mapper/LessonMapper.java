package com.kirillk.appointmentapi.mapper;

import com.kirillk.appointmentapi.dto.LessonDto;
import com.kirillk.appointmentapi.entity.Lesson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TeacherMapper.class, StudentMapper.class})
public interface LessonMapper {

    @Mapping(target = "teacher", source = "teacherDto")
    Lesson toEntity(LessonDto workTimeDto);

    @Mapping(target = "teacherDto", source = "teacher")
    LessonDto toDto(Lesson workTime);

    List<LessonDto> toDtoList (List<Lesson> workTimeList);
}
