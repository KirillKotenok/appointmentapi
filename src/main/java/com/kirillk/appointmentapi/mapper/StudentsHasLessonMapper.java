package com.kirillk.appointmentapi.mapper;

import com.kirillk.appointmentapi.dto.StudentsHasLessonDto;
import com.kirillk.appointmentapi.entity.StudentsHasLesson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {LessonMapper.class, StudentMapper.class})
public interface StudentsHasLessonMapper {

    StudentsHasLessonDto toDto(StudentsHasLesson studentsHasLesson);

    StudentsHasLesson toEntity(StudentsHasLessonDto studentsHasLessonDto);

    List<StudentsHasLessonDto> toDtoList (List<StudentsHasLesson> studentsHasLessonList);
}
