package com.kirillk.appointmentapi.mapper;

import com.kirillk.appointmentapi.dto.TeacherDto;
import com.kirillk.appointmentapi.entity.Teacher;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {LessonMapper.class})
public interface TeacherMapper {

    Teacher toEntity (TeacherDto teacherDto);

    TeacherDto toDto (Teacher teacher);

    List<TeacherDto> toDtoList(List<Teacher> teacherList);
}
